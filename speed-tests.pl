# speed-tests.pl

my $start1 = time();
foreach(1..1000){
	my $infocmp = `infocmp`;
	$infocmp =~ /cols#(\d+)/ or die "no cols in infocmp";
	my $cols = $1;
	$infocmp =~ /lines#(\d+)/ or die "no lines in infocmp";
	my $lines = $1;
	print "$cols x $lines\n";
}
my $dur1 = time() - $start1;




my $start2 = time();
foreach(1..1000){
	my $cols = `tput cols`;
	my $lines = `tput lines`;
	chomp($cols);
	chomp($lines);
	print "$cols x $lines\n";
}
my $dur2 = time() - $start2;

print "$dur1 vs $dur2\n";
