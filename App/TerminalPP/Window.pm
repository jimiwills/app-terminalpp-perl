package App::TerminalPP::Window;
use strict;
use warnings;
use Carp;

# create a new object... maybe as a child of an existing one...
sub new {
	my $p = shift;
	my $c = ref($p) || $p;
	my %defs = qw(
		isroot 0
	);
	my $o = {%defs, @_};
	bless $o, $c;
	if(ref($p)){
		$o->parent->add($o);
	}
	else {
		$o->{'isroot'} = 1;
		$o->{'buffer'} = '';
		$o->{'iscontainer'} = 1;
	}
	return $o;
}

sub add { # add a child to this...
	my $parent = shift;
	my $child = shift; # child
	die "$parent is not a container"
		unless $parent->iscontainer;
	$child->{'parent'} = $parent;
	push @{$parent->{'children'}}, $child;
	return $child;
}

sub iscontainer {
	$_[0]->{'iscontainer'} = $_[1] if @_ > 1;
	return $_[0]->{'iscontainer'};
}

sub isroot {
	my $o = shift;
	return $o->{'isroot'};
}

# get dimensions for this
sub getdims {
	my $o = shift;
	my @dims;
	if($o->isroot){
		@dims = (0,0,App::TerminalPP::size());
	}
	else {
		@dims = $o->layoutdim;
	}
	($o->{'x'}, 
	 $o->{'y'}, 
	 $o->{'w'}, 
	 $o->{'h'} ) = @dims;
	return @dims;
}

# obsolute position....
sub getabsxy {
	my $o = shift;
	if($o->isroot){
		return (0,0);
	}
	my ($x,$y) = $o->parent->getabsxy;
	$x += $o->{'x'};
	$y += $o->{'y'};
	return ($x,$y);
}

# return dimensions for the given child
sub layout {
	my ($o,$c) = @_;
	my $l = $c->{'layout'};
	# here we can figure out child's x,y,w,h
	# how?
	
}

=cut

Layout

layout mechanisms:

"auto" shrinks the dims to the contents and is the default
"fill" grows the dims to the container and is default for app length

app [nsewc] <depth> <length>

nsew is north, south, east, west or centre.  
depth is width for e,w, height for others
length is height for e,w, width for others
default depth is always auto (shrink)
default length is always fill (grow)

flow <width> <height>

defaults for width and height are auto (shrink)
will flow the children top-bottom, left-right

abs x y w h

places the windows at x, y. 
defaults for width and height are auto (shrink)
defaults for x,y are 0,0

abscentre w,h

centres the window in the terminal
defaults for w,h are auto (shrink)


=cut



# grab this object's layout (via the parent)
sub layoutdim {
	my $o = shift;
	return $o->parent->layout($o);
}

# return the parent
sub parent {
	return $_[0]->{'parent'};
}

# refresh the text in the buffer...
sub refresh {
	my $o = shift;
	$o->getdims;
	my ($w,$h) = ($o->{'h'},$o->{'w'});
	my $line = "$w x $h " . '#' x $o->{'w'};
	$line .= "\n";
	$o->{'buffer'} = $line x $h;
}

# put a string
sub Z {
	shift @_ if ref $_[0];
	App::TerminalPP::put(@_);
}
# send a command
sub X {
	shift @_ if ref $_[0];
	App::TerminalPP::cmd(@_);
}

# paint the text into the correct position
# this is robust to weirdness, except for tabs
sub repaint {
	my $o = shift;
	my ($x,$y) = ($o->{'x'},$o->{'y'});
	my ($w,$h) = ($o->{'w'},$o->{'h'});
	my @lines = split /\n/, $o->{'buffer'};
	my $r = $y+1;
	my $c = $x+1;
	my $cmd = 'H';
	my $esc = chr(27).'[';
	foreach my $line(@lines){
		my $L = length($line);
		$line = substr($line, 0, $w) if $L > $w;
		$line .= ' ' x $w - $L if $L < $w;
		X('cup', $r, $c);
		Z($line);
		$r++;
	}
}
sub flush {
	$App::TerminalPP::TTY->flush();
}
sub loop {
	App::TerminalPP::loop();
}



1;

__END__

Layouts are confusing!

What we need to be able to do, is to layout from the top (root window), 
or request (re)layout from the bottom (label, etc)
Request from the bottom just tells top to do it
But top then has to query each item to find out what dimensions they request
and then calculate what it can give them.
Each container window needs to to that for all it's children, some of
which might be containers themselves.  Then the details get passed up
finally to root window.
Root windows can then tell each child what dims it's getting.  Containers
can then tell their chilren what they're getting, and so on.

Required methods:

request_repaint() : from child to parent; stops at root window
tell_dim_needs() : called by parent on children; 
	dim needs can be empty, request or demand, heigh, width or both
	demand one, request the other, etc.
	a cool request/demand is for area... for text windows that require
	to display a certain number of characters... area is all they
	need (if you ignore wrap; probably add 10% for wrap)
	another request could be fill.
assign_dims() : called by parent on children, tells them what they're getting
repaint() : called by parent on children, makes them paint themselves

typically, a label would demand 1 line, demand 5 cols, request cols to fit
its text into.

If demands clash, need to be democratic about it...

demand 1 row 5 cols request 5 rows fill cols 20 area

to accumulate your demands and requests (as a container)
simply add up the rows and cols demanded and requested.

assuming 4/1 aspect, add sqrt(area)/2 to rows and sqrt(area)*2 to cols.
This allows passing a reasonably request to the parent. (does it?)

Of course, we only need to do this per "row" of windows and take the
maxima for demands and requests.  So it all depends on the layout style.

And, of course, absolutely positioned windows are completely exempt, as
they simply overlay previous windows.

Once you've applied for the necessary width/length/area, you'll find out
what you're actually getting.  Root (or absolutely positioned) window
desides based on what's available.  Then you can decide what your children
should get, based on demands, requests, and respective position in the
layout (and so on)











