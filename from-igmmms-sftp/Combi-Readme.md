# Combi Readme

Combi.pl is a script for making a choice of few or many items.
Will paint a title, a list of items and give an entry line.
Text entered into the entry line is used to filter the items.
Items can then be chosen based on navigating by arrows.

When user presses enter, the selected item is written out.
Then the script exits.

This could be used to choose from a list of servers, experiments,
projects, or even just a short list of actions.
