package App::TerminalPP;

use strict;
use warnings;
use Data::Dumper;
use Carp;
use IO::File;
use App::TerminalPP::Window;
use App::TerminalPP::Label;

## we should have Term::ReadKey on all systems

use Term::ReadKey;

our $RMKX = '';
our $TTY;
our %TC = (); # terminal capabilities
our %CI = ();

############
# Get terminal capabilities and known keys

# warning: infocmp must be available!

%CI = map {/=/ ? split(/=/, $_, 2) : ()} map {s/\\E/\e/g;$_} grep /\\E/, split /,\s+/, `infocmp`;
#	print `infocmp`;
%TC = (morekeys(), %CI, reverse(%CI),
		chr(0x00) => 'ctrl-`', # NUL null (ctrl-space works too)
		chr(0x01) => 'ctrl-a', # SOH start of heading
		chr(0x02) => 'ctrl-b', # STX start of text
		chr(0x03) => 'ctrl-c', # ETX end of text
		chr(0x04) => 'ctrl-d', # EOT end of transmission
		chr(0x05) => 'ctrl-e', # ENQ enquiry
		chr(0x06) => 'ctrl-f', # ACK acknowledge
		chr(0x07) => 'ctrl-g', # BEL bell
		chr(0x08) => 'ctrl-h', # BS backspace
		chr(0x09) => 'tab', # TAB horizontal tab (or ctrl-i)
		chr(0x0a) => 'ctrl-j', # LF (\n, line feed)
		chr(0x0b) => 'ctrl-k', # VT vertical tab
		chr(0x0c) => 'ctrl-l', # FF (NP form feed/new page)
		chr(0x0d) => 'enter', # CR carriage return
		chr(0x0e) => 'ctrl-n', # SO shift out
		chr(0x0f) => 'ctrl-o', # SI shift in
		chr(0x10) => 'ctrl-p', # DLE data link escape
		chr(0x11) => 'ctrl-q', # DC1 device control 1
		chr(0x12) => 'ctrl-r', # DC2
		chr(0x13) => 'ctrl-s', # DC3
		chr(0x14) => 'ctrl-t', # DC4
		chr(0x15) => 'ctrl-u', # NAK negative acknowledge
		chr(0x16) => 'ctrl-v', # SYN synchronous idle
		chr(0x17) => 'ctrl-w', # ETB end of trans. block
		chr(0x18) => 'ctrl-x', # CAN cancel
		chr(0x19) => 'ctrl-y', # EM end of medium
		chr(0x1a) => 'ctrl-z', # SUB substitute
		chr(0x1b) => 'esc', # ESC escape (or ctrl-[ )
		chr(0x1c) => 'ctrl-\\', # FS file separator
		chr(0x1d) => 'ctrl-]', # GS group separator
		chr(0x1e) => 'ctrl-~', # RS record separator
		chr(0x1f) => 'ctrl-/', # US unit separator
		chr(0x7f) => 'backspace',
		"\eOk" => '<k+>',
		"\eOm" => '<k->',
		"\eOj" => '<k*>',
		"\eOo" => '<k/>',
		
);

sub morekeys {
	my %directions = qw(
		A	Up
		B	Down
		C	Right
		D	Left
		E	Middle
	);
	my %padkeys = qw(
		k	+
		m	-
		j	*
		o	/
	);
	my %combos = qw(
		2	shift
		3	alt
		4	shift-alt
		5	ctrl
		6	shift-ctrl
		7	ctrl-alt
		8	shift-ctrl-alt
	);
	my %km = ();
	foreach my $c(keys %combos){
		my $cl = $combos{$c};
		foreach my $d(keys %directions){
			my $dl = $directions{$d};
			my $es = "\e[1;$c$d";
			my $name = "$cl-$dl";
			$km{$es} = $name;	
		}
		foreach my $p(keys %padkeys){
			my $pk = $padkeys{$p};
			my $es = "\eO$c$p";
			my $name = "$cl-$pk";
			$km{$es} = $name;
		}
	}
	return %km;
}


$RMKX = $CI{'rmkx'};
$TTY = IO::File->new('/dev/tty', 'w') or die $!;
binmode($TTY);
print $TTY $TC{'smkx'}; # App mode
ReadMode 5; # raw mode


END {
	# RESTORE CONSOLE MODE
	ReadMode 0;
	print $TTY $RMKX;
}


# go hot
$|++;


sub size {
	# grab terminal size
	my ($wc,$hc,$wp,$hp) = GetTerminalSize();
	return ($wc,$hc);
}

=cut

	The actual keys we might get are:

	kdch1 - del
	kich1 - insert
	kf1-12 - function keys
	kcuu1 - up
	kcuf1 - forward (right)
	kcub1 - back (left)
	kcud1 - down
	khome - home
	kpp - page up (previous page)
	knp - page down (next page)
	kend - end


	backspace
	enter
	tab
	esc
	ctrl a-z (excl i,j)
	ctrl-[
	ctrl-\
	ctrl-]
	ctrl-/
	ctrl-`
	
	might also get <esc>a <esc>b etc as user can do that and we can find it.
	esc-m gives <memu>
	esc-l gives <meml>
	esc-8 gives <rc>
	esc-7 gives <sc>
	
	Obviously, that will be terminal dependent.
	
=cut

# probably we should call loop on a window, not the app...

our $currentwindow;

# create and return the root window
sub createroot {
	return focus(App::TerminalPP::Window->new());
}

# check for a terminal capability
sub can {
	return exists $CI{$_[0]};
}

# send a command to the terminal
# this check the command is available
# also, will replace %p1%d etc with things from @_
#
# now allows 2nd argument "yield" to return the code
sub cmd {
	my $yield = 0;
	if($_[0] eq 'YIELD'){
		shift;
		$yield = 1;
	}
	my $cmd = $_[0];
	if(can($cmd)){
		my $p = $CI{$cmd};
		$p =~ s/(?:\%i|)\%p(\d+)\%d/$_[$1]/ge;
		$yield 
		  ? return $p # yield codes to calling function
		  : put($p);
	}
	else {
		die "terminal capabilities: ".Dumper(\%CI);
	}
}



# put a string to the terminal
sub put {
	print $TTY @_;
}

# set focus to the given window
sub focus {
	$currentwindow = $_[0];
	return $currentwindow;
}

# loop - give any keys received to key() of the window in focus
sub loop {
	while(1){
		my $k = readkey();
		next unless defined $k;
		last if $k eq '<esc>';
		$currentwindow->key($k);
	}
}

# read a key. 
# if necessary, intelligently put together several chars to make a key
sub readkey {
	my $key = keynb();
	return unless defined $key;
	if(isesc($key)){
		my $next = keynb();
		$key .= $next if defined $next;
		while(! exists $TC{$key}){
			my $next = keynb();
			last unless defined $next;
			$key .= $next;
#			'print "[$key]\n";
			return if length($key)>7;
		}
	}
	return "<$TC{$key}>" if exists $TC{$key};
	$key =~ s/\e/<esc>/g;
	return $key;
}
# check if a thing is escape
sub isesc {
	shift @_ if ref $_[0];
	return $_[0] eq "\e";
}
# non-blocking get key
sub keynb {
	my $k = ReadKey 1;
	return unless defined $k;
	#print unpack("H*", $k),"\n";
	return $k;
}




1;


