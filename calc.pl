#!/usr/bin/perl
use strict;
use warnings;
use lib '.';
use App::TerminalPP;
use Text::CSV;

our $FILENAME = '';
our $MODE = 'ready';
our $COMMANDBUFFER = '';

our @CURSOR = (1,1,1,1); 

our %MODES = (
	'ready' => {
		':' => \&modeCommand,
		'<kcub1>' => \&cursorLeft,
		'<kcuf1>' => \&cursorRight,
		'<kcuu1>' => \&cursorUp,
		'<kcud1>' => \&cursorDown,
		'<kLFT>' => \&cursorLeftSelect,
		'<kRIT>' => \&cursorRightSelect,
		'<kri>' => \&cursorUpSelect,
		'<kind>' => \&cursorDownSelect,
		'<default>' => \&setStatus
	},
	'command' => {
		'<esc>' => \&modeReady,
		'<default>' => \&commander,
		'<enter>' => \&runCommand,
	},
);

our %COMMANDS = (
	':q' => \&quit,
);

sub quit {
	setStatus("quit!");
	exit;
}

sub cursorSingle {
	$CURSOR[1] = $CURSOR[0];
	$CURSOR[3] = $CURSOR[2];
}

sub cursorLeft {
	$CURSOR[2]--;
	cursorSingle();
	refreshFileView();
}
sub cursorRight {
	$CURSOR[2]++;
	cursorSingle();
	refreshFileView();
}
sub cursorDown {
	$CURSOR[0]++;
	cursorSingle();
	refreshFileView();
}
sub cursorUp {
	$CURSOR[0]--;
	cursorSingle();
	refreshFileView();
}


sub cursorLeftSelect {
	$CURSOR[3]--;
	refreshFileView();
}
sub cursorRightSelect {
	$CURSOR[3]++;
	refreshFileView();
}
sub cursorDownSelect {
	$CURSOR[1]++;
	refreshFileView();
}
sub cursorUpSelect {
	$CURSOR[1]--;
	refreshFileView();
}

sub X { return App::TerminalPP::cmd(@_); } # send a command
sub C { return App::TerminalPP::cmd('YIELD',@_); } # retreive a command
sub Z {	return App::TerminalPP::put(@_); } # put some text
sub F {	return $App::TerminalPP::TTY->flush(); } # flush buffer
sub S { return App::TerminalPP::size(); } # width,height
sub R { return App::TerminalPP::readkey(); } # read a key


sub mode {
	my ($m, $k) = @_;
	die "no such mode: $m" unless exists $MODES{$m};
	if(exists $MODES{$m}->{$k}){
		&{$MODES{$m}->{$k}}($k);
	}
	elsif(exists $MODES{$m}->{'<default>'}){
		&{$MODES{$m}->{'<default>'}}($k);
	}
	else {
		setStatusRight('not implemented');
	}
}

sub bottomLine{
	my ($w,$h) = App::TerminalPP::size();
	return $h;
}

sub setStatusRight {
	my ($v) = @_;
	my $L = length($v);
	my ($h,$w) = S();
	putCursor($h, $w-$L);
	Z($v);
}

sub putCursor {
	my ($y, $x) = @_;
	X('cup',$y, $x);
}

sub setStatus {
	my $value = shift;
	my $y = bottomLine();
	my $x = 1;
	putCursor($y, $x);
	X('el');
	Z($value);
	F();
}

sub modeCommand {
	my ($first) = @_;
	$MODE = 'command';
	$COMMANDBUFFER = $first;
	setStatus($COMMANDBUFFER);
}

sub modeReady {
	$MODE = 'ready';
	setStatus('ready');
}

sub commander {
	my ($key) = @_;
	if(length($key) == 1){
		$COMMANDBUFFER .= $key;
		setStatus($COMMANDBUFFER);
	}
}

sub runCommand {
	if(exists $COMMANDS{$COMMANDBUFFER}){
		modeReady();
		&{$COMMANDS{$COMMANDBUFFER}}($COMMANDBUFFER);
	}
	else {
		modeReady();
		setStatus("unrecognised command: $COMMANDBUFFER");
	}
}

sub loop {
	while(1){
		my $k = R();
		if(defined $k){
			mode($MODE, $k);
		}
	}
}

sub cropcell {
	my ($v,$l) = @_;
	$v = " " x ($l - length($v)) . $v if length($v) < $l;
	return $v if length($v) <= $l;
	return substr($v, 0, $l-1).'$';
}

sub fileview {
	my ($fn, $x, $y, $colwidth, %opts) = @_;

	# decide what type of file:
	my $sep = "\t";
	if($fn =~ /\.csv$/i){
		$sep = ",";
	}
	my $csv = Text::CSV->new ( { binary => 1, sep_char=>$sep } )  
          or die "Cannot use CSV: ".Text::CSV->error_diag ();
	
  	# we might also want some intelligent buffering at some point...
	#
  	my $fh = IO::File->new($fn,'r');
	# start simple, we'll hard-code freezing
	# the topmost row and leftmost column...
	# $x,$y says what cell is in the top-left
	my ($sw,$sh) = S();
	$sw --;

	# for first row and col frozen (only!)
	$x = 1 if $x < 1;
	$y = 1 if $y < 1;

	# grab header, and some other infos
	my @header = @{$csv->getline($fh)};
	my $dw = @header;

	# collect info about data dimensions...
	my @columnstarts = (0);
	my $posn = exists $opts{'colwidth0'} ? $opts{'colwidth0'} : $colwidth;
	my @colwidths = ($posn);
	my @i = (0);
	foreach my $i($x..$dw){
		push @columnstarts, $posn;
		push @i, $i;
		my $w = exists $opts{"colwidth$i"} 
			? $opts{"colwidth$i"} : $colwidth;

		$posn += $w;

		if($posn >= $sw){
			$w -= ($posn - $sw);
		}
		push @colwidths, $w;

		last if $posn >= $sw;
	}
	my $lastrow = $sh - 3 + $y;
	my @j = (0,$y..$lastrow);
	
	# construct a text representing the view on the data...

	my $text = cropcell(
		join('', map {cropcell($header[$_],$colwidths[$_])} @i),
		$sw);


	my $j = 0;


	my %formats = ();
	my ($x1,$x2) = $CURSOR[2] < $CURSOR[3] ? @CURSOR[2,3] : @CURSOR[3,2];
	my ($y1,$y2) = $CURSOR[0] < $CURSOR[1] ? @CURSOR[0,1] : @CURSOR[1,0];
	foreach my $cursorY($y1..$y2){
		foreach my $cursorX($x1..$x2){
			my $posn = "$cursorY,$cursorX";
			$formats{$posn} = ['smso','rmso'];
		}
	}

	while($j<$lastrow){
		last if $fh->eof();
		$j++;
		my @f = @{$csv->getline($fh)};
		$text .= "\r\n";
		my @values = map {cropcell($f[$_],$colwidths[$_])} @i;
		foreach my $i(0..$#values){
			my $posn = "$j,$i";
			my @fmt = ('','');
			if(defined $formats{$posn}){
				@fmt = map {C($_)} @{$formats{$posn}};
			}
			$text .= $fmt[0].$values[$i].$fmt[1];
		}
		#$text .= join '', map {cropcell($f[$_],$colwidths[$_])} @i;
	}

	X('clear');
	Z($text);
	setStatus("$dw columns");
	F();
}

sub putcursor {
	# here we need to figure out how to highlight
	# a cell and do that... or something.
	#
	# smso to start "standout mode" and rmso to finish
	# I have checked this and it works on gitbash too
}



sub paint {
}

if(@ARGV){
	foreach $FILENAME(@ARGV){

	}
}

paint();

sub refreshFileView {
	fileview($ARGV[0],0,0,8,());
}

X('clear');
modeReady();
fileview($ARGV[0],0,0,8,());
loop();



