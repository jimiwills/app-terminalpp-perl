# console-app-perl

The aim of this project is to make a Pure Perl Curses/Curses::UI-like app framework that will run equally well on Linux and Git-Bash on Windows.  These are personal aims.

Ultimately, my aim is for one example app to be a terminal spreadsheet program.

With tha App stuff in mind, I have been trying to make sense of layouts, etc.  But actually, for my finaly aim of a spreadsheet app, all I need are basically vertically tiled windows, 3 of which are simply single lines:

Title
Worksheet
Status
Command line

If we want to be vim-like, we might got for JUST THE COMMAND LINE!
Worksheet
Command/Status/Title line

This offers the best freedom and functionality.
We can show:

* colname/rowid of the current cell
* col/row index
* any formulae
* current mode
* command prompt in command mode

We need:

* not the windows in the App at all.
* a status line object
* an App  object that can change modes, and redirect to the correct
mode.  
* objects for each mode.
* an object for the grid display/navigation
* mode objects can pass keys to the grid object
* grid object manipulates text to show 
	* frozen cols/rows
	* navigated cols/rows
	* interpretations based on formulae

Modes of saving/opening
This is designed for tsv.  Might do csv, but I do not see a massive need as
I primarily want to look at MaxQuant output.
Results will live in the tabular data
Formulae will live at the bottom of the file, and could operate more like R
commands that deal with whole columns at once, than columns of individual
formulae that do not.







