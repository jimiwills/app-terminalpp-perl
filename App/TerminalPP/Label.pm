package App::TerminalPP::Label;
BEGIN {
	push @ISA, 'App::TerminalPP::Window';
}

sub new {
	my $p = shift;
	my $c = ref($p) || $p;
	my $o = App::TerminalPP::Window->new(@_);
	bless $o, $c;
	return $o;
}

sub App::TerminalPP::Window::Label {
	my $w = shift;
	return $w->add(App::TerminalPP::Label->new(@_));
}

1;


