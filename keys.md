# Arrows

	<direction>
	A <kcuu1> Up
	B <kcud1> Down
	C <kcuf1> Right
	D <kcub1> Left

Control/Alt

	<esc>[1;5<direction>  Ctrl-<direction>
	<esc>[1;3<direction>  Alt-<direction>
	<esc>[1;7<direction>  Ctrl-Alt-<direction>
	E.g. <esc>[1;7D  Ctrl-Alt-Left

Shift

	<kLFT>
	<kRIT>

(up and down do things in the terminal)

Combinations of Ctrl, Alt and Shift with arrow keys are generally bound
to the windowing system.


# Nav keys

    <kich1>     Ins
    <kdch1>     Del
    <khome>     Home
    <kend>      End
    <kpp>       Page Up
    <knp>       Page Down

                Ctrl-Ins
    <esc>[3;5~  Ctrl-Del
    <esc>[1;5H  Ctrl-Home
    <esc>[1;5F  Ctrl-End
    <esc>[5;5~  Ctrl-Page-Up
    <esc>[6;5~  Ctrl-Page-Down

    <esc>[2;3~  Alt-Ins
    <esc>[3;3~  Alt-Del
    <esc>[1;3H  Alt-Home
    <esc>[1;3F  Alt-End
    <esc>[5;3~  Alt-Page-Up
    <esc>[6;3~  Alt-Page-Down

    <esc>[2;7~  Ctrl-Alt-Ins
                Ctrl-Alt-Del
    <esc>[1;7H  Ctrl-Alt-Home
    <esc>[1;7F  Ctrl-Alt-End
    <esc>[5;7~  Ctrl-Alt-Page-Up
    <esc>[6;7~  Ctrl-Alt-Page-Down

Home and End mostly just give khome and kend on Webtop. Is it the keyboard
or is it linux? Does it matter?  Also, the PgUP PgDn keys with shift are
bound to terminal window functions.  Everything else seems to work the same
though.


# Numberpad (with Num lock off)

    <kb2>       Middle (5)
    <esc>Ok     +
    <esc>Om     -
    <esc>Oj     *
    <esc>Oo     /
    <kent>      Enter

With ctrl...

    <esc>[1;5<direction>

In this context, middle is E.
or

    <esc>O2j is Shift-* etc
    <esc>O3j is Alt-* etc
    <esc>O4j is Shift-Alt-* etc
    <esc>O5j is Ctrl-* etc
    <esc>O6j is Shift-Ctrl-* etc
    <esc>O7j is Ctrl-Alt-* etc
    <esc>O8j is Shift-Ctrl-Alt-* etc

Some of this isn't the same on lenovo laptop accessing windows git bash 
via vnc.

The + and - keys tend to be bound to terminal sizing/navigation operations.

# Other stuff

    Ctrl-Enter => <ctrl-~>
    Shift-Enter => <ctrl-j>
    Shift-Backspace => <ctrl-/>
    Alt-Backspace => <esc>

Shift-Enter is the same on Linux, but the reset are not. 

Pretty much all other combinations with Enter and Backspace do weird stuff


    Pause|Break => <ctrl-]>

    <kf1> .. <kf12> Function keys

    F10 does things in linux terminal (menu)


Tab is obviously <tab>
on Webtop linux Shift-Tab gives <cbt>. Is this also true on Windows?
Yes, it does seem that <cbt> is also Shift-tab on Windows git bash.


