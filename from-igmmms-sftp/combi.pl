#!/usr/bin/perl

# USAGE:
# cat file | ./combi.pl -t title > answer
# ./combi.pl -t title filein > answer
# you can omit the title option

use strict;
use warnings;
use Data::Dumper;

## we should have Term::ReadKey on all systems
use Term::ReadKey;

### set default title option
my %opts = ('-t'=>join(' ',@ARGV) || 'combi.pl');
### get any options
while(@ARGV > 1 && $ARGV[0] =~ /^-/){
	my $k = shift @ARGV;
	my $v = shift @ARGV;
	$opts{$k} = $v;
}
# set the title from the options
my $title = $opts{'-t'};

## define some variables to do with geometry, etc
my $padh = 2; # top and bottom
my $padw = 5; # left and right
my $selectedtext = '<none>';
my $selectedline = 0;

## what to show if there are not results...
my $noresultstring = '[no results]';

### read in the data (from stdin, always)
my @data = <>;

# now we've got the data...
# open term explicitly to get keys, 
# and assign to STDIN

close(STDIN);
open(STDIN, '<', '/dev/tty') or die $!;

# open stdout at the tty
my $therealstdout = *STDOUT;
open(my $newstdout, '>', '/dev/tty') or die $!;
*STDOUT = $newstdout;

# binary mode
binmode(STDIN);
binmode(STDOUT);

### grab the terminal's capabilities
my %tc = (termcap(), knownkeys());

# set application mode and ultra-raw readmode
my $smkx = $tc{'smkx'};
print $smkx;
ReadMode 5; # raw mode

# go hot
$|++;

# get a couple other items
my $titlepre = $tc{'smso'};
my $titlepost = $tc{'rmso'};
my $selectpre = '  '.$tc{'smso'};
my $selectpost = $tc{'rmso'};
my $defaultpre = '  ';
my $defaultpost = '';

# grab terminal size
my ($wc,$hc,$wp,$hp) = GetTerminalSize();

# A note... it's in a string because leafpad wouldn't ignore it properly in cut
my $note = q!

	The actual keys we might get are:

	kdch1 - del
	kich1 - insert
	kf1-12 - function keys
	kcuu1 - up
	kcuf1 - forward (right)
	kcub1 - back (left)
	kcud1 - down
	khome - home
	kpp - page up (previous page)
	knp - page down (next page)
	kend - end


	backspace
	enter
	tab
	esc
	ctrl a-z (excl i,j)
	ctrl-[
	ctrl-\
	ctrl-]
	ctrl-/
	ctrl-`
	
	might also get <esc>a <esc>b etc as user can do that and we can find it.
	esc-m gives <memu>
	esc-l gives <meml>
	esc-8 gives <rc>
	esc-7 gives <sc>
	
	Obviously, that will be terminal dependent.
	

!;

#

# remove line ending chars
foreach my $i(0..$#data){
	$data[$i] =~ s/[\n\r]+//g;
}

# default filter...
my $filter = '';
my @filtered = @data;

# call update to paint the terminal
update();

while(1){
	my $k = readkey();
	next unless defined $k;
	last if $k eq '<esc>';
	if(length($k) == 1){ # just a regular character
		$selectedline = 0; # reset index
	  $filter .= $k;
		update();
	}
	elsif($k eq '<backspace>'){
		# this will need to get more intelligent when we do line editing
		$filter = substr($filter, 0, length($filter)-1);
		update();
	}
	elsif($k eq '<kcuu1>'){
		$selectedline ++; # update will fix this if it's too high
		update();
	}
	elsif($k eq '<kcud1>'){
		$selectedline --; # update will fix this if it's too low
		update();
	}
	elsif($k eq '<enter>'){
		print $therealstdout $selectedtext
			unless $selectedtext eq $noresultstring;
		exit;
	}
#	else {
#		print "$k";
#	}
}

sub dofilter {
	@filtered = @data;
	return @filtered unless $filter;
	my @parts = split /\s+/, $filter;
	foreach my $part(@parts){
		print STDERR "$part\r\n";
		eval { @filtered = grep /$part/i, @filtered;};
	}
	@filtered = ($noresultstring)
		if @filtered == 0;
}

sub update {
	dofilter();
  ($wc,$hc,$wp,$hp) = GetTerminalSize();
  my $linecount = $hc - $padh;
	my @show = @filtered;
	while(@show < $linecount){
		push @show, '';
	}
	while(@show > $linecount){
		pop @show;
	}
	# before we reverse, check that there's an item at this line
	#  this is not so trivial as there might be blanks within the list
	# so start from the end and find the first non-blank
	my $eol = 0;
	for(my $i=$#show; $i>=0; $i--){
		if($show[$i] =~ /\w/){
			$eol = $i+1;
			last;
		}
	}
	$selectedline %= $eol;
	# now reverse to hits start near the input line...
	@show = reverse @show;
	print $tc{'clear'};
	print $titlepre,$title,$titlepost,"\r\n";
	my $itemlength = $wc - $padw;
	foreach my $i(0 .. $#show){
		my $item = $show[$i];
		my $pre = $defaultpre;
		my $post = $defaultpost;
		if($linecount - $selectedline - 1 == $i){
			$selectedtext = $item;
			$pre = $selectpre;
			$post = $selectpost;
		}
		while(length($item) < $itemlength){
			$item .= ' ';
		}
		
		print $pre, substr($item, 0, $wc-$padw), $post, "\r\n";
	}
	print '> ',$filter;
}


sub termcap {
	my %ci = map {/=/ ? split(/=/, $_, 2) : ()} map {s/\\E/\e/g;$_} grep ! /\%/, grep /\\E/, split /,\s+/, `infocmp`;
	return (%ci, reverse %ci);
}

sub knownkeys {
	return (
		chr(0x00) => 'ctrl-`', # NUL null (ctrl-space works too)
		chr(0x01) => 'ctrl-a', # SOH start of heading
		chr(0x02) => 'ctrl-b', # STX start of text
		chr(0x03) => 'ctrl-c', # ETX end of text
		chr(0x04) => 'ctrl-d', # EOT end of transmission
		chr(0x05) => 'ctrl-e', # ENQ enquiry
		chr(0x06) => 'ctrl-f', # ACK acknowledge
		chr(0x07) => 'ctrl-g', # BEL bell
		chr(0x08) => 'ctrl-h', # BS backspace
		chr(0x09) => 'tab', # TAB horizontal tab (or ctrl-i)
		chr(0x0a) => 'ctrl-j', # LF (\n, line feed)
		chr(0x0b) => 'ctrl-k', # VT vertical tab
		chr(0x0c) => 'ctrl-l', # FF (NP form feed/new page)
		chr(0x0d) => 'enter', # CR carriage return
		chr(0x0e) => 'ctrl-n', # SO shift out
		chr(0x0f) => 'ctrl-o', # SI shift in
		chr(0x10) => 'ctrl-p', # DLE data link escape
		chr(0x11) => 'ctrl-q', # DC1 device control 1
		chr(0x12) => 'ctrl-r', # DC2
		chr(0x13) => 'ctrl-s', # DC3
		chr(0x14) => 'ctrl-t', # DC4
		chr(0x15) => 'ctrl-u', # NAK negative acknowledge
		chr(0x16) => 'ctrl-v', # SYN synchronous idle
		chr(0x17) => 'ctrl-w', # ETB end of trans. block
		chr(0x18) => 'ctrl-x', # CAN cancel
		chr(0x19) => 'ctrl-y', # EM end of medium
		chr(0x1a) => 'ctrl-z', # SUB substitute
		chr(0x1b) => 'esc', # ESC escape (or ctrl-[ )
		chr(0x1c) => 'ctrl-\\', # FS file separator
		chr(0x1d) => 'ctrl-]', # GS group separator
		chr(0x1e) => 'ctrl-~', # RS record separator
		chr(0x1f) => 'ctrl-/', # US unit separator
		chr(0x7f) => 'backspace',
	);
}

sub readkey {
	my $key = keynb();
	return unless defined $key;
	if(isesc($key)){
		my $next = keynb();
		$key .= $next if defined $next;
		while(! exists $tc{$key}){
			my $next = keynb();
			last unless defined $next;
			$key .= $next;
#			'print "[$key]\n";
			return if length($key)>7;
		}
	}
	return "<$tc{$key}>" if exists $tc{$key};
	$key =~ s/\e/<esc>/g;
	return $key;
}
sub isesc {
	return $_[0] eq "\e";
}
sub keynb {
	my $k = ReadKey 1;
	return unless defined $k;
	#print unpack("H*", $k),"\n";
	return $k;
}

END {
	ReadMode 0;
	print $tc{'rmkx'};
}
