# updated notes

I think there needs to be a file-interation part that can load a chunk at a time,
and also ignore columns as requested (i.e. don't read that field into the available
chunk)

Then when some other part of the program asks for a range of lines/columns, the
file interaction part will do what's necessary and hand back the data.

This means the file interaction part can also incorporate funky file formats as
the project matures.

 
The entry of formulae??  It really would be great to have the thing work JUST
like excel/calc/gnumeric.




# notes on how I want my spreadsheet to function 

select with shift doesn't work in windows git bash because shift arrows
are bound to console nav.  therefore, perhaps, vi-like bindings?

v to enter visual select, and in that mode the arrows move the other corner
how to multiple select?
what are the formulae going to look like
how are formulae to be stored?  I quite like the idea of adding them
as formulae at the bottome


ooh!  I just had the idea that the whole thing could interface with R as necessary

I need to make the current functionality into an object, so more than one document
can be open

use a command to switch between documents.


check how it currently works for large files... maybe modularise the functions of
grabbing bits of file and presenting them?  Maybe?  Could be okay as it is.

... or potentially keep the file index in a variable so it's quicker to scan
back to that place in the file... 




I feel like I want to optimise the front end too... so that it only repaints
the parts of the console that need it?

But this could be for later on, as I don't sense any issue at the moment,
bearing in mind that each time I move the cursor I'm reading the file, 
processing, formatting, highlighting and writing... It's not noticeable.

However, perhaps I can introduce a buffer so that it's possible to optimise
later.  And hold rows in memory so that horizontal and some vertical navigation
is possible before reloading.  How much?  Should be in config and args.

## Parameters required in a calc object:

filename
bytes/lines to buffer
line numbers buffered
start/end line numbers of buffer displayed
file offsets of lines buffered
cache of previous buffer-file offsets (for rapid navigation)

probably the file navigation should be in a separate object?

list of columns displayed - not necessarily consecutive or in order
fixed columns/rows
column widths
list of selections
cursor position


## Storage models

there are a number of ways storage could work...

* read/write formulae & import/export values as tsv/csv:
    - read/write formulae to csv/tsv
    - read/write xml or other custom format
* tsv/csv with trailer of formulae/commands/history
* save both tables in one tsv/csv
    - formulae first, with trailer of calculated version
    - save calculated values first, with trailer of formulae
* export to fixed width
    - view
    - buffer
    - file
    - current graph? 

Also could support ods!  Well, partially.  How are we gonna make graphs?

## Graphing

a graph can be shown in ascii, but better in unicode...

.-'`




