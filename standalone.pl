use strict;
use warnings;
use Data::Dumper;
use Carp;
use IO::File;
use Fcntl;
use Errno;
use Data::Dumper;

###########################################################################
#
#	Globals? Yuck!   - it's a standalone script, deal with it!
#
###########################################################################

our $RMKX = ''; 
our $TTY;
our %TC = (); # terminal capabilities
our %CI = ();
our $UICXT = "NUI";
our $CMDBUF = '';
our @CMDHISTORY = ();
our $STTY_G = "";


###########################################################################
#
#	KEYBOARD COMMAND MAP
#
###########################################################################

our %KEYACTIONS = (
	'context:key' => 'action',  # ... that how this works

	# context changers 
	'NUI::' => '_setCEM', # set command entry mode
	'CEM:<esc>' => '_setNUI', # set navigation user interface
	'CEM:<enter>' => '_CEMrun', # run the current command

	# default handlers...
	'CEM:' => '_CEMinput',
	':' => 'noop', # default handler
	'NUI:' => 'print',

	# context free (overwritable in context)
	':<ctrl-q>' => 'exitnow', # just quit right now, all contexts unless overridden in any

	# context-specific
	'NUI:t' => 'timenow',
	'NUI:q' => 'currenttest' 
);





###########################################################################
#
#	COMMANDS (do I keep hints separate or put them in here?)
#
###########################################################################


our %COMMANDS = (
	# underscore indicates the command should not be called directly
	'_setCEM' => \&setCEM, # set command entry mode
	'_setNUI' => \&setNUI, # set nav ui mode
	'_CEMrun' => \&CEMrun, # run current command

	'_CEMinput' => sub {length($_[0])>1 ? CEMctrl(@_) : CEMchar(@_) }, # default handler

	'noop' => sub {}, # "no operation" / do nothing

	'exitnow' => sub{exit},
	'timenow' => sub {print "\r".localtime()."  ";},
	'print' => sub {print "\r$_[0]         ";},

	'currenttest' => sub {}
);


###########################################################################
#
#	Code to run this thing...
#
###########################################################################

# warning: infocmp must be available!
setup_capabilities();
setmodeUI();

uiloop();

restoreconsolemode(); # this is called in END block anyway





###########################################################################
#
#	UI LOOP
#
###########################################################################


sub uiloop {
 	while(1){
 		my $k = readkey();
 		next unless defined $k;
 		my $cmd;
 		### look for the command in the current context
 		if(exists $KEYACTIONS{"$UICXT:$k"}){
 			$cmd = $KEYACTIONS{"$UICXT:$k"};
 		}
 		### look for the contextless command 
 		elsif(exists $KEYACTIONS{":$k"}){
 			$cmd = $KEYACTIONS{":$k"};
 		}
 		### look for a default key handler in the current context
 		elsif(exists $KEYACTIONS{"$UICXT:"}){
 			$cmd = $KEYACTIONS{"$UICXT:"};
 		}
 		### look for a contextless default key handler
 		elsif(exists $KEYACTIONS{":"}){
 			$cmd = $KEYACTIONS{":"};
 		}
 		else {
 			die "no ticket!";
 		}
 		### check that the command found is in the command list
 		die "command '$cmd' does not exist" 
 			unless exists $COMMANDS{$cmd};
 		my $code = $COMMANDS{$cmd};
 		### grab and run the coderef
 		if(ref($code) eq 'CODE'){
			&{$COMMANDS{$cmd}}($k, $UICXT);
 		}
 		else {
 			die "running non-coderef commands is not implemented ($code)"
 		}
 		### exit loop during development:
 		last if $k eq 'x';
	}
}



###########################################################################
#
#	MODE CHANGING
#
###########################################################################


sub setCEM {
	$UICXT = "CEM";
	print "\r$_[0]";

	$CMDBUF = $_[0];
}

sub setNUI {
	$UICXT = "NUI";
}




###########################################################################
#
#	COMMAND FUNCTIONS
#
###########################################################################


sub CEMctrl {
	print "\ncontrol char $_[0] in $_[1]\n";
}

sub CEMchar {
	print $_[0];
	$CMDBUF .= $_[0];
}

sub CEMrun {
	print "\rrun $CMDBUF ... not implemented";
	cmdhistadd($CMDBUF);
	setNUI();
}


###########################################################################
#
#	COMMAND HISTORY FUNCTIONS
#
###########################################################################


sub cmdhistadd {
	unshift @CMDHISTORY, $_[0];
	pop @CMDHISTORY if @CMDHISTORY > 100;
}







###########################################################################
#
#	SETUP FUNCTIONS
#
###########################################################################


sub setup_capabilities {
	%CI = run_infocmp();
	%TC = (morekeys(), %CI, reverse(%CI), keydefs());
}


sub run_infocmp {
	return map {/=/ ? split(/=/, $_, 2) : ()} map {s/\\E/\e/g;$_} grep /\\E/, split /,\s+/, `infocmp`;
}

sub keydefs {
	return (
		chr(0x00) => 'ctrl-`', # NUL null (ctrl-space works too)
		chr(0x01) => 'ctrl-a', # SOH start of heading
		chr(0x02) => 'ctrl-b', # STX start of text
		chr(0x03) => 'ctrl-c', # ETX end of text
		chr(0x04) => 'ctrl-d', # EOT end of transmission
		chr(0x05) => 'ctrl-e', # ENQ enquiry
		chr(0x06) => 'ctrl-f', # ACK acknowledge
		chr(0x07) => 'ctrl-g', # BEL bell
		chr(0x08) => 'ctrl-h', # BS backspace
		chr(0x09) => 'tab', # TAB horizontal tab (or ctrl-i)
		chr(0x0a) => 'ctrl-j', # LF (\n, line feed)
		chr(0x0b) => 'ctrl-k', # VT vertical tab
		chr(0x0c) => 'ctrl-l', # FF (NP form feed/new page)
		chr(0x0d) => 'enter', # CR carriage return
		chr(0x0e) => 'ctrl-n', # SO shift out
		chr(0x0f) => 'ctrl-o', # SI shift in
		chr(0x10) => 'ctrl-p', # DLE data link escape
		chr(0x11) => 'ctrl-q', # DC1 device control 1
		chr(0x12) => 'ctrl-r', # DC2
		chr(0x13) => 'ctrl-s', # DC3
		chr(0x14) => 'ctrl-t', # DC4
		chr(0x15) => 'ctrl-u', # NAK negative acknowledge
		chr(0x16) => 'ctrl-v', # SYN synchronous idle
		chr(0x17) => 'ctrl-w', # ETB end of trans. block
		chr(0x18) => 'ctrl-x', # CAN cancel
		chr(0x19) => 'ctrl-y', # EM end of medium
		chr(0x1a) => 'ctrl-z', # SUB substitute
		chr(0x1b) => 'esc', # ESC escape (or ctrl-[ )
		chr(0x1c) => 'ctrl-\\', # FS file separator
		chr(0x1d) => 'ctrl-]', # GS group separator
		chr(0x1e) => 'ctrl-~', # RS record separator
		chr(0x1f) => 'ctrl-/', # US unit separator
		chr(0x7f) => 'backspace',
		"\eOk" => '<k+>',
		"\eOm" => '<k->',
		"\eOj" => '<k*>',
		"\eOo" => '<k/>',
	);
}

sub morekeys {
	my %directions = qw(
		A	Up
		B	Down
		C	Right
		D	Left
		E	Middle
	);
	my %padkeys = qw(
		k	+
		m	-
		j	*
		o	/
	);
	my %combos = qw(
		2	shift
		3	alt
		4	shift-alt
		5	ctrl
		6	shift-ctrl
		7	ctrl-alt
		8	shift-ctrl-alt
	);
	my %km = ();
	foreach my $c(keys %combos){
		my $cl = $combos{$c};
		foreach my $d(keys %directions){
			my $dl = $directions{$d};
			my $es = "\e[1;$c$d";
			my $name = "$cl-$dl";
			$km{$es} = $name;	
		}
		foreach my $p(keys %padkeys){
			my $pk = $padkeys{$p};
			my $es = "\eO$c$p";
			my $name = "$cl-$pk";
			$km{$es} = $name;
		}
	}
	return %km;
}




###########################################################################
#
#	MODE FUNCTIONS
#
###########################################################################


sub setmodeUI {
	$RMKX = $CI{'rmkx'};
	sysopen($TTY, "/dev/tty", O_NONBLOCK|O_RDWR) or die "Can't open tty: $!\n";
	binmode($TTY);
	tty_print($TC{'smkx'}); # App mode
	stty_raw(); # raw mode		
	# go hot
	$|++;
}

sub restoreconsolemode {
	# RESTORE CONSOLE MODE
	stty_restore();
	tty_print($RMKX);
}

END {
	# RESTORE CONSOLE MODE
	restoreconsolemode();
}





###########################################################################
#
#	SOME NOTES ON KEYS
#
###########################################################################


=cut

	The actual keys we might get are:

	kdch1 - del
	kich1 - insert
	kf1-12 - function keys
	kcuu1 - up
	kcuf1 - forward (right)
	kcub1 - back (left)
	kcud1 - down
	khome - home
	kpp - page up (previous page)
	knp - page down (next page)
	kend - end


	backspace
	enter
	tab
	esc
	ctrl a-z (excl i,j)
	ctrl-[
	ctrl-\
	ctrl-]
	ctrl-/
	ctrl-`
	
	might also get <esc>a <esc>b etc as user can do that and we can find it.
	esc-m gives <memu>
	esc-l gives <meml>
	esc-8 gives <rc>
	esc-7 gives <sc>
	
	Obviously, that will be terminal dependent.
	
=cut



###########################################################################
#
#	TERMINAL CONTROL
#
###########################################################################

# NEWSFLASH: we do not need Term::ReadKey...
#use Term::ReadKey;

sub stty_restore {
	# definitely restore console to a usable state:
	`stty cooked onlcr iexten echo echoe echok echoctl echoke -noflsh`;
	# hopefully restore console to its previous state:
	`stty $STTY_G`;
	# "unset" $STTY_G so later calls to stty_raw() will work:
	$STTY_G = ""
}

sub stty_raw {
	# never overwrite $STTY_G ... stores stty settings
	return if $STTY_G;
	$STTY_G = `stty -g`;
	# set the desired level of rawness:
	`stty raw raw -onlcr -iexten -echo -echoe -echok -echoctl -echoke noflsh`;
}

# this next lot is adapted from: https://docstore.mik.ua/orelly/perl4/cook/ch07_21.htm

sub tty_print {
	my $buffer = join '', @_;
	my $rv = syswrite($TTY, $buffer, length $buffer);
 	if (!defined($rv) && $!{EAGAIN}) { 
 		# would block 
 	} elsif ($rv != length $buffer) { 
 		# incomplete write 
 		die "tty_print(): incomplete write";
 	} else { 
 		# successfully wrote 
 	} 
}

sub tty_readchar {
	my $buffer = '';
	my $rv = sysread($TTY, $buffer, 1); 
	if (!defined($rv) && $!{EAGAIN}) { 
		# would block 
	} else { 
		# successfully read $rv bytes from HANDLE 
		return $buffer;
	} 
}



###########################################################################
#
#	TERMINAL FUNCTIONS
#
###########################################################################
#
# calling stty every now and then is okay, 
# but continuously calling tput would be very slow...
#


sub terminal_size {
	# grab terminal size
	# using infocmp here as it's quicker this way than 2 calls to tput
	my $infocmp = `infocmp`; 
	$infocmp =~ /cols#(\d+)/ or die "no cols in infocmp";
	my $cols = $1;
	$infocmp =~ /lines#(\d+)/ or die "no lines in infocmp";
	my $lines = $1;
	return ($cols, $lines);
}

# check for a terminal capability
sub terminal_can {
	return exists $CI{$_[0]};
}

# send a command to the terminal
# this check the command is available
# also, will replace %p1%d etc with things from @_
#
# now allows 1nd argument "YIELD" to return the code
sub terminal_cmd {
	my $yield = 0;
	if($_[0] eq 'YIELD'){
		shift;
		$yield = 1;
	}
	my $cmd = $_[0];
	if(terminal_can($cmd)){
		my $p = $CI{$cmd};
		$p =~ s/(?:\%i|)\%p(\d+)\%d/$_[$1]/ge;
		$yield 
		  ? return $p # yield codes to calling function
		  : tty_print($p);
	}
	else {
		die "terminal capabilities: ".Dumper(\%CI);
	}
}





###########################################################################
#
#	KEY READING FUNCTIONS
#
###########################################################################


# read a key. 
# if necessary, intelligently put together several chars to make a key
sub readkey {
	my $key = keynb();
	return unless defined $key;
	if(isesc($key)){
		my $next = keynb();
		$key .= $next if defined $next;
		while(! exists $TC{$key}){
			my $next = keynb();
			last unless defined $next;
			$key .= $next;
#			'print "[$key]\n";
			return if length($key)>7;
		}
	}
	return "<$TC{$key}>" if exists $TC{$key};
	$key =~ s/\e/<esc>/g;
	return $key;
}
# check if a thing is escape
sub isesc {
	shift @_ if ref $_[0];
	return $_[0] eq "\e";
}
# non-blocking get key
sub keynb {
	my $k = tty_readchar();
	return unless defined $k;
	#print unpack("H*", $k),"\n";
	return $k;
}




