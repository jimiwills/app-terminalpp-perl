#!/usr/bin/perl
use strict;
use warnings;
use lib '.';
use App::TerminalPP;

my $mw = App::TerminalPP::createroot();

App::TerminalPP::put( "normal ");
App::TerminalPP::cmd("smso");
App::TerminalPP::put("standout");
App::TerminalPP::cmd("rmso");
App::TerminalPP::put(" normal again");
exit;

$mw->X('clear');

$mw->Label('text'=>'Hello world!');

$mw->refresh();
$mw->repaint();
$mw->flush();

$mw->loop();




